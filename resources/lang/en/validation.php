<?php
return [
    'unknown_error' => [
        'code' => '0',
        'message' => 'Unknown error'
    ],
    'structure' => [
        'code' => '1',
        'message' => 'Betslip structure mismatch'
    ],
    'distinct' => [
        'code' => '8',
        'message' => 'Duplicate selection found'
    ],
	'locking' => 'Your previous action is not finished yet',
	'insufficient_balance' => 'Insufficient balance',
	'max_win' => 'Maximum win amount is ' . \App\Bet::MAX_WIN,
	'required' => [
		'code' => '12',
		'message' => ':attribute field is required'
	],
    'custom' => [
        'stake_amount' => [
            'min' => [
                'code' => '2',
                'message' => 'Minimum :attribute is :min.'
            ],
            'max' => [
                'code' => '3',
                'message' => 'Maximum :attribute is :max.'
            ]
        ],
        'selections' => [
            'min' => [
                'code' => '4',
                'message' => 'Minimum number of :attribute is :min.'
            ],
            'max' => [
                'code' => '5',
                'message' => 'Maximum number of :attribute is :max.'
            ]
        ],
        'selections.*.odds' => [
			'min' => [
				'code' => '6',
				'message' => 'Minimum :attribute are :min.'
			],
			'max' => [
				'code' => '7',
				'message' => 'Maximum :attribute are :max.'
			]
		],
		
    ],
];
