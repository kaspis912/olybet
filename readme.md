## Kasparas Olybet task
- Used Lumen for a lighweight solution

# How to run the application
- composer install
- setup .env for database
- php artisan migrate
- php -S localhost:8000 -t public

# Api testing
- To test long processes pass a 'sleep' variable in the body of the /api/bet 
- Without the sleep variable it will not test long process.

## Limitations
# In a real app I would have:
- Implemented an Authentication system that uses OAuth.
- After request validation, transform the array into Bet DTO or something similar. If the api input fields change, we would not need to change a lot of code this way.
- For balance in the database, would use integers instead of decimals. Store balance in cents and could use a money package to have helpful methods getting the balance and manipulating the money.

### Style check

```
$ vendor/bin/php-cs-fixer fix --verbose
```