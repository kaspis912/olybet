<?php

namespace App\Exceptions;

class LockingException extends TranslatableWithErrorCodeException
{
    public function getErrorCode(): int
    {
        return 10;
    }

    public function getTranslationKey(): string
    {
        return 'validation.locking';
    }
}
