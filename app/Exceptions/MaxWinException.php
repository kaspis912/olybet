<?php

namespace App\Exceptions;

class MaxWinException extends TranslatableWithErrorCodeException
{
    public function getErrorCode(): int
    {
        return 9;
    }

    public function getTranslationKey(): string
    {
        return 'validation.max_win';
    }
}
