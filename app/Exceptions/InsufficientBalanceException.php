<?php

namespace App\Exceptions;

class InsufficientBalanceException extends TranslatableWithErrorCodeException
{
    public function getErrorCode(): int
    {
        return 11;
    }

    public function getTranslationKey(): string
    {
        return 'validation.insufficient_balance';
    }
}
