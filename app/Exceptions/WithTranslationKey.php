<?php

namespace App\Exceptions;

interface WithTranslationKey
{
    public function getTranslationKey(): string;
}
