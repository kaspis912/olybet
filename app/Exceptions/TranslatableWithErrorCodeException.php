<?php

namespace App\Exceptions;

use RuntimeException;

abstract class TranslatableWithErrorCodeException extends RuntimeException implements WithErrorCode, WithTranslationKey
{
}
