<?php

namespace App\Exceptions;

interface WithErrorCode
{
    public function getErrorCode(): int;
}
