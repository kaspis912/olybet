<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Locking\LockingInterface;
use App\Services\Locking\EloquentBetModelLocking;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(LockingInterface::class, EloquentBetModelLocking::class);
    }
}
