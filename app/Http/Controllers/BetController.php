<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Validator as IlluminateValidator;
use App\Services\MakeBetService;
use App\Services\Locking\LockingInterface;
use App\Exceptions\TranslatableWithErrorCodeException;
use App\Exceptions\LockingException;

class BetController extends ApiController
{
    private $rules = [
        'stake_amount' => 'required|numeric|min:0.3|max:10000',
        'selections' => 'required|array|min:1|max:20',
        'selections.*.odds' => 'required|numeric|min:1|max:10000',
        'selections.*.id' => 'required|distinct',
        'player_id' => 'required',
    ];
    
    private $betService;
    private $locking;
    
    public function __construct(MakeBetService $betService, LockingInterface $locking)
    {
        $this->betService = $betService;
        $this->locking = $locking;
    }
    
    public function makeBet(Request $request)
    {
        $validator = IlluminateValidator::make($request->all(), $this->rules);

        $playerId = $request->input('player_id');
        
        if ($validator->fails()) {
            return $this->respondWithValidationError($validator);
        }

        if ($this->locking->isLocked($playerId)) {
            return $this->respond($this->makeResponseError(new LockingException()), 400);
        }

        try {
            $this->betService->makeBet($request->all());
        } catch (TranslatableWithErrorCodeException $e) {
            $this->locking->unlock($playerId);

            return $this->respond($this->makeResponseError($e), 400);
        }

        $this->locking->unlock($playerId);
        
        return $this->respond(['success']);
    }
}
