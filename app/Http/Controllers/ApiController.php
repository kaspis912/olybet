<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Exceptions\TranslatableWithErrorCodeException;

class ApiController extends Controller
{
    protected $statusCode = 201;
    protected $errorCode = 400;

    protected function respondWithValidationError($validator): Response
    {
        return $this->respond([
            'errors' => $validator->errors()->toArray(),
        ], $this->errorCode);
    }

    protected function respond(array $data, int $statusCode = 201, array $headers = []): Response
    {
        return (new Response($data, $statusCode, $headers));
    }

    protected function makeResponseError(TranslatableWithErrorCodeException $exception): array
    {
        return [
            'code' => $exception->getErrorCode(),
            'message' => __($exception->getTranslationKey()),
            'success' => false,
        ];
    }
}
