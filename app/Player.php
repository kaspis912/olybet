<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'balance', 'betting_locked'
    ];
    
    public $incrementing = false;

    public function bets()
    {
        return $this->hasMany(Bet::class);
    }
}
