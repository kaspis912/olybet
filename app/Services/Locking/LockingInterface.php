<?php

namespace App\Services\Locking;

interface LockingInterface
{
    public function isLocked(int $playerId): bool;

    public function lock(int $playerId): bool;

    public function unlock(int $playerId): bool;
}
