<?php

namespace App\Services\Locking;

use App\Bet;
use App\Player;

class EloquentBetModelLocking implements LockingInterface
{
    public function isLocked(int $playerId): bool
    {
        $player = Player::find($playerId);

        if (! $player) {
            return false;
        }

        return $player->betting_locked;
    }

    public function lock(int $playerId): bool
    {
        $player = Player::find($playerId);

        if (! $player) {
            return false;
        }

        $player->betting_locked = true;
        $player->save();

        return true;
    }

    public function unlock(int $playerId): bool
    {
        $player = Player::find($playerId);

        if (! $player) {
            return false;
        }

        $player->betting_locked = false;
        $player->save();

        return true;
    }
}
