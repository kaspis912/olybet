<?php

namespace App\Services;

use App\Player;
use App\Bet;
use App\BalanceTransaction;
use App\BetSelection;
use App\Services\Locking\LockingInterface;
use App\Exceptions\MaxWinException;
use App\Exceptions\InsufficientBalanceException;

class MakeBetService
{
    private $locking;

    public function __construct(LockingInterface $locking)
    {
        $this->locking = $locking;
    }
    
    public function makeBet(array $input): bool
    {
        $player = Player::find($input['player_id']);

        if (!$player) {
            $player = $this->createPlayer($input);
            $player->refresh();
        }
        
        $this->locking->lock($player->id);
        
        if ($player->balance < $input['stake_amount']) {
            throw new InsufficientBalanceException();
        }
        
        if ($this->calculateMaxWin($input) > Bet::MAX_WIN) {
            throw new MaxWinException();
        }

        $bet = $this->createBet($input, $player);

        // Pass sleep variable in POST to simulate long process
        if (array_key_exists('sleep', $input)) {
            sleep(rand(1, 30));
        }
        
        $this->createBetSelections($input, $bet);
        $balanceTransaction = $this->createBalanceTransaction($input, $player);
        
        $this->updatePlayerBalance($player, $balanceTransaction);

        return true;
    }
    
    private function createPlayer(array $input): Player
    {
        $player = Player::create([
            'id' => $input['player_id'],
            'betting_locked' => true,
        ]);

        return $player;
    }
    
    private function createBet(array $input): ?Bet
    {
        return Bet::create([
            'player_id' => $input['player_id'],
            'stake_amount' => $input['stake_amount']
        ]);
    }
    
    private function createBetSelections(array $input, Bet $bet): Void
    {
        foreach ($input['selections'] as $selection) {
            BetSelection::create([
                'bet_id' => $bet->id,
                'selection_id' => $selection['id'],
                'odds' => $selection['odds'],
            ]);
        }
    }

    private function createBalanceTransaction(array $input, Player $player): BalanceTransaction
    {
        return BalanceTransaction::create([
            'player_id' => $player->id,
            'amount_before' => $player->balance,
            'amount' => $player->balance - $input['stake_amount'],
        ]);
    }

    private function updatePlayerBalance(Player $player, BalanceTransaction $transaction): Player
    {
        $player->balance = $transaction->amount;
        $player->save();

        return $player;
    }
    
    private function calculateMaxWin(array $input): int
    {
        $maxWin = $input['stake_amount'];

        foreach ($input['selections'] as $selection) {
            $maxWin = $selection['odds'] * $maxWin;
        }

        return $maxWin;
    }
}
