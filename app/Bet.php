<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    protected $fillable = [
        'stake_amount', 'player_id'
    ];
    
    const MAX_WIN = 20000;
}
